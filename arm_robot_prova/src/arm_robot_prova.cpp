/*********************************************************************
Inspired by move_it_tutorials. https://github.com/ros-planning/moveit_tutorials/blob/master/doc/move_group_interface/src/move_group_interface_tutorial.cpp
 *********************************************************************/

#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <moveit_visual_tools/moveit_visual_tools.h>

// The circle constant tau = 2*pi. One tau is one rotation in radians.
const double tau = 2 * M_PI;

// Function that gets the robot position and show it with visual_tools.
void update_joint_positions(
			    const moveit::core::JointModelGroup* joint_model_group,
			    moveit::planning_interface::MoveGroupInterface& move_group_interface,
			    moveit_visual_tools::MoveItVisualTools& visual_tools
			   ){

  namespace rvt = rviz_visual_tools;
  moveit::core::RobotStatePtr current_state = move_group_interface.getCurrentState();
  std::vector<double> joint_group_positions;
  current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);

  // Concatenate the string that output the positions
  std::string joint_pos = "Joint positions [degrees]:\n";
  for(int i=0; i<7; i++){
    std::string cur_angle = std::to_string(joint_group_positions[i] * (180.0/M_PI));
    cur_angle = cur_angle.substr(0, cur_angle.find(".")+2);
    joint_pos = joint_pos + "Joint[" + std::to_string(i) + "]: " +  cur_angle + "\n";
  }
  
  Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
  text_pose.translation().z() = 1.0;
  visual_tools.deleteAllMarkers();
  visual_tools.publishText(text_pose, joint_pos.c_str(), rvt::WHITE, rvt::XLARGE);
  visual_tools.trigger();

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "move_group_interface_tutorial");
  ros::NodeHandle node_handle;

  // ROS spinning must be running for the MoveGroupInterface to get information
  // about the robot's state. One way to do this is to start an AsyncSpinner
  // beforehand.
  ros::AsyncSpinner spinner(1);
  spinner.start();

  moveit_visual_tools::MoveItVisualTools visual_tools("panda_link0");
  std::string PLANNING_GROUP = "panda_arm";
  moveit::planning_interface::MoveGroupInterface move_group_interface(PLANNING_GROUP);

  // Raw pointers are frequently used to refer to the planning group for improved performance.
  const moveit::core::JointModelGroup* joint_model_group =
      move_group_interface.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

  // For loop to repeat the 3 times
  for(unsigned int i=0; i<3; i++){ 
    update_joint_positions(joint_model_group, move_group_interface, visual_tools);
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    moveit::core::RobotStatePtr current_state = move_group_interface.getCurrentState();

    // Next get the current set of joint values for the group.
    std::vector<double> joint_group_positions;
    current_state->copyJointGroupPositions(joint_model_group, joint_group_positions);
  
    // Decide which direction to turn depending on joint 0 position
    if(joint_group_positions[0] <= 0)
      joint_group_positions[0] = joint_group_positions[0] + tau / 6; // add 60 degrees
    else
      joint_group_positions[0] = joint_group_positions[0] - tau / 6; // remove 60 degrees
    
    move_group_interface.setJointValueTarget(joint_group_positions);
    
    // Control the speed 
    move_group_interface.setMaxVelocityScalingFactor(0.5);
    move_group_interface.setMaxAccelerationScalingFactor(0.5);
    
    bool success = (move_group_interface.plan(my_plan) == moveit::core::MoveItErrorCode::SUCCESS);
    // If can do the action -> move 
    if(success)
      move_group_interface.move();

  } // end for loop
  
  ros::shutdown();
  return 0;
}
